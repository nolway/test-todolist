<?php

namespace App\Service;

use App\Entity\User;

class MailService
{

    public static function send(User $user): bool
    {
        return $user->getAge() >= 18;
    }
}