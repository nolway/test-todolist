<?php

namespace App\Tests;

use App\Entity\Item;
use App\Entity\ToDoList;
use PHPUnit\Framework\TestCase;

class ToDoListTest extends TestCase
{
    public function testCanAddItemTrue()
    {
        $list = new ToDoList();
        $user = $this->getMockBuilder('App\Entity\User')
            ->setMethods(['getAge'])->getMock();
        $user->method("getAge")->will($this->returnValue(18));

        $list->setOwner($user);
        $itemGood = new Item();
        $itemGoodNext = new Item();
        $itemGood->setCreatedAt(new \DateTime('-1 hours'))
            ->setContent('Hello')
            ->setName('test1');
        $itemGoodNext->setCreatedAt(new \DateTime())
            ->setContent('Hello')
            ->setName('test2');
        $list->addItem($itemGood);

        $this->assertTrue(!is_null($list->addItem($itemGoodNext)));
    }

    public function testCanAddItemFalse()
    {
        $list = new ToDoList();
        $user = $this->getMockBuilder('App\Entity\User')
            ->setMethods(['getAge'])->getMock();
        $user->method("getAge")->will($this->returnValue(18));

        $list->setOwner($user);
        $itemGood = new Item();
        $itemGoodNext = new Item();
        $itemGood->setCreatedAt(new \DateTime('-25 minutes'))
            ->setContent('Hello')
            ->setName('test1');
        $itemGoodNext->setCreatedAt(new \DateTime())
            ->setContent('Hello')
            ->setName('test2');
        $list->addItem($itemGood);

        $this->assertFalse(!is_null($list->addItem($itemGoodNext)));
    }

}
